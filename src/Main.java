import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter non-negative integer number for which you want to calculate array of factorials:");
        ArrayList<BigInteger> result;
        while (true) {
            try {
                int n = scanner.nextInt();
                result = FactorialCalculator.getFactorialList(n);
                break;
            } catch (Exception e) {
                scanner.nextLine();
                System.out.println("An error occurred: " + (e.getMessage() != null ? e.getMessage() : "invalid input"));
                System.out.println("Please, try entering non-negative integer number again:");
            }
        }
        System.out.println("Array of factorials:");
        for (int i = 0; i < result.size(); i++) {
            System.out.println(i + "! = " + result.get(i));
        }
    }
}