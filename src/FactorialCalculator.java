import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;

public class FactorialCalculator {
    public static ArrayList<BigInteger> getFactorialList(int n) throws IllegalArgumentException {
        if (n < 0) throw new IllegalArgumentException("Amount of elements must be non-negative");
        ArrayList<BigInteger> result = new ArrayList<>(n + 1);
        Iterator<BigInteger> iterator = getFactorialIterator(n);
        for (int i = 0; i <= n; i++) result.add(iterator.next());
        return result;
    }

    public static Iterator<BigInteger> getFactorialIterator(int limit) throws IllegalArgumentException {
        if (limit < 0) throw new IllegalArgumentException("Amount of elements must be non-negative");
        if (limit == 0) return new Iterator<>() {
            private boolean hasNext = true;

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public BigInteger next() {
                hasNext = false;
                return BigInteger.ONE;
            }
        };
        return new Iterator<>() {
            private int index = 0;
            private BigInteger current = BigInteger.ONE;

            @Override
            public boolean hasNext() {
                return index < limit;
            }

            @Override
            public BigInteger next() {
                BigInteger result = current;
                index += 1;
                current = current.multiply(BigInteger.valueOf(index));
                return result;
            }
        };
    }

    public static Iterator<BigInteger> getInfiniteFactorialIterator() {
        return new Iterator<>() {
            private int index = 0;
            private BigInteger current = BigInteger.ONE;

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public BigInteger next() {
                BigInteger result = current;
                index += 1;
                current = current.multiply(BigInteger.valueOf(index));
                return result;
            }
        };
    }
}
