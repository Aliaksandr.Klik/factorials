import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class FactorialCalculatorTest {

    @Test
    void getFactorialListNegative() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> FactorialCalculator.getFactorialList(-1));
        assertEquals("Amount of elements must be non-negative", exception.getMessage());
    }

    @Test
    void getFactorialListZero() {
        ArrayList<BigInteger> expected = new ArrayList<>(1);
        expected.add(BigInteger.ONE);
        ArrayList<BigInteger> actual = FactorialCalculator.getFactorialList(0);
        assertEquals(expected, actual);
    }

    @Test
    void getFactorialListTen() {
        int[] expectedInts = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
        ArrayList<BigInteger> expected = FactorialCalculator.getFactorialList(10);
        for (int i = 0; i < expectedInts.length; i++) {
            assertEquals(BigInteger.valueOf(expectedInts[i]), expected.get(i));
        }
    }

    @Test
    void getFactorialIteratorNegative() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> FactorialCalculator.getFactorialIterator(-1));
        assertEquals("Amount of elements must be non-negative", exception.getMessage());
    }

    @Test
    void getFactorialIteratorZero() {
        Iterator<BigInteger> iterator = FactorialCalculator.getFactorialIterator(0);
        assertTrue(iterator.hasNext());
        assertEquals(BigInteger.ONE, iterator.next());
        assertFalse(iterator.hasNext());
    }

    @Test
    void getFactorialIteratorTen() {
        Iterator<BigInteger> iterator = FactorialCalculator.getFactorialIterator(10);
        int[] expectedInts = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
        for (int expectedInt : expectedInts) {
            assertTrue(iterator.hasNext());
            assertEquals(BigInteger.valueOf(expectedInt), iterator.next());
        }
        assertFalse(iterator.hasNext());
    }

    @Test
    void getInfiniteFactorialIterator() {
        Iterator<BigInteger> iterator = FactorialCalculator.getInfiniteFactorialIterator();
        int[] expectedInts = {1, 1, 2, 6, 24, 120, 720, 5040, 40320};
        for (int expectedInt : expectedInts) {
            assertTrue(iterator.hasNext());
            assertEquals(BigInteger.valueOf(expectedInt), iterator.next());
        }
        assertTrue(iterator.hasNext());
    }
}